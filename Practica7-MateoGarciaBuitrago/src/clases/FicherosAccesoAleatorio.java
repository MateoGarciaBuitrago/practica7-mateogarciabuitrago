package clases;

/**
 * Clase para usar los ficheros aleatorios
 */
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

public class FicherosAccesoAleatorio {

	/**
	 * Unicamente creo un ArrayList donde almacenar la informacion de las entradas
	 */

	private ArrayList<Entrada> v;

	/**
	 * Constructor de la clase
	 */
	public FicherosAccesoAleatorio() {
		v = new ArrayList<Entrada>();
	}

	/**
	 * A partir de aqui, los metodos trabajaran con el fichero aleatorio
	 */
	public void rellenarLista() {
		@SuppressWarnings("resource")
		Scanner lector = new Scanner(System.in);
		String respuesta = "";
		do {

			Entrada entrada = new Entrada();

			entrada.rellenarEntrada();

			v.add(entrada);
			System.out.println("Quieres continuar (si/no)?");
			respuesta = lector.nextLine();

		} while (respuesta.equalsIgnoreCase("si"));

	}

	public void visualizarLista() {
		for (Entrada entrada : v) {
			entrada.visualizarEntrada();
		}
	}

	public void copiarListaArchivo(String nombreArchivo) {
		try {

			RandomAccessFile f = new RandomAccessFile(nombreArchivo, "rw");

			f.seek(f.length());

			for (Entrada entrada : v) {
				f.writeInt(entrada.getCodigo());
				f.writeDouble(entrada.getPrecio());
				f.write(entrada.getDni());
				System.out.println("Datos introducidos correctamente");
			}

			f.close();

		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
	}

	public void visualizarArchivo(String archivo) {
		int codigo;
		double precio;
		int dni;

		try {

			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			boolean finFichero = false;

			do {
				try {
					codigo = (int) f.readInt();
					precio = (double) f.readDouble();
					dni = (int) f.readInt();
					System.out.println("Codigo " + codigo);
					System.out.println("Precio " + precio);
					System.out.println("Dni " + dni);
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error de entrada de datos");
			System.exit(0);
		}

	}

	public void modificarPrecioArchivo(String archivo) {

		double precio;
		double descuento = 0.21;
		double cargoGestion = 1.05;
		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");

			boolean finFichero = false;
			do {
				try {

					precio = (double) f.readDouble();
					if (precio > 30) {
						f.seek(f.getFilePointer() - 8);
						f.writeDouble(precio - precio * descuento);
					} else {
						f.seek(f.getFilePointer() - 8);
						f.writeDouble(precio + precio * cargoGestion);
					}
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
			System.exit(0);
		}
	}

	public void mensajeFinal(String archivo) {

		@SuppressWarnings("resource")
		Scanner lector = new Scanner(System.in);
		String mensaje = "Entrada premiada con viaje a Cancun, Felicidades!";

		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			mensaje = lector.nextLine();
			f.seek(f.length());
			f.writeUTF(mensaje);
			f.close();

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}

	public void mensajeInicio(String archivo) {
		@SuppressWarnings("resource")
		Scanner lector = new Scanner(System.in);
		String mensaje;

		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			System.out.print("Introduce el mensaje de bienvenida");
			mensaje = lector.nextLine();
			f.seek(0);
			f.writeUTF(mensaje);
			f.close();

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}

}
