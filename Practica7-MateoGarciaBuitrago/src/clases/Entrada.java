package clases;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Entrada {
	private int codigo;
	private double precio;
	private int dni;

	public Entrada() {
		this.codigo = 0;
		this.precio = 0;
		this.dni=0;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public void rellenarEntrada() {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		boolean error = true;

		do {
			try {
				System.out.println("");
				System.out.println("Generador automatico de entradas");
				System.out.println("Este asistente le acompaņara para que introduzca los datos pertinentes");
				System.out.print("Introduzca su Codigo de entrada = ");
				this.codigo = Integer.parseInt(in.readLine());;
				System.out.print("Introduzca el Precio de la entrada = ");
				this.precio = Double.parseDouble(in.readLine());
				System.out.print("Introduzca por ultimo su Dni = ");
				this.dni = Integer.parseInt(in.readLine());
				System.out.println("");
				error = false;
			} catch (NumberFormatException e) {
				System.out.println("Datos mal introducidos");
				error = true;
			} catch (IOException e) {
				System.out.println("Fallo en los datos");
				error = true;
			}

		} while (error);

	}

	public void visualizarEntrada() {
		System.out.println("Codigo "+this.codigo);
		System.out.println("Precio "+this.precio);
		System.out.println("Precio "+this.dni);
	}

}
