package clases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

public class FicherosSecuenciales {

	static Scanner lector = new Scanner(System.in);

	private String archivo;

	public FicherosSecuenciales(String archivo) {
		this.archivo = archivo;
	}

	public void crearArchivo() throws IOException {
		System.out.println("CREAR ARCHIVO");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String linea;

		PrintWriter fuente = new PrintWriter(new BufferedWriter(new FileWriter(this.archivo)));
		System.out.println("Creacion de un archivo");
		System.out.println("Cada registro de informacion debe de situarse en una linea");
		System.out.println("Introduzca la palabra clave NOMBRE y detras su nombre completo");
		System.out.println("Introduzca la palabra clave CODIGO y detras el codigo de su entrada");
		System.out.println("Introduzca la palabra clave LUGAR y detras el emplazamiento del concierto");
		System.out.println("Introduzca la palabra clave FESTIVAL y detras el nombre del festival");
		System.out.println("Introducir / para finalizar");
		linea = in.readLine();

		while (!linea.equalsIgnoreCase("/")) {
			fuente.println(linea);
			linea = in.readLine();
		}
		System.out.println("Archivo creado");
		fuente.close();

	}

	public void visualizarArchivo() throws IOException {
		String linea;
		System.out.println("Visualizar el archivo " + this.archivo);
		BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
		linea = fuente.readLine();
		while (linea != null) {
			System.out.println(linea);
			linea = fuente.readLine();
		}
		fuente.close();
	}

	public void buscarFichero() {

		String valorABuscar;
		String linea;
		System.out.println("BUSCAR PALABRA EN EL VECTOR");

		System.out.println("Introduce la palabra a buscar");
		valorABuscar = lector.nextLine();
		Vector<String> v = new Vector<String>();

		try {
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			linea = fuente.readLine();
			while (linea != null) {
				String letra;
				String palabra = "";
				for (int j = 0; j < linea.length(); j++) {
					letra = linea.substring(j, j + 1);
					if (letra.equalsIgnoreCase(" ") == false) {
						palabra = palabra + letra;
					}
				}
				v.add(palabra);
				palabra = "";
				linea = fuente.readLine();
			}
			int contadorPalabras = 0;
			for (int j = 0; j < v.size(); j++) {
				String valor = v.elementAt(j).toString();
				if (valor.equalsIgnoreCase(valorABuscar) == true) {
					contadorPalabras++;
					System.out.println("valor encontrado -> " + valorABuscar);
				}
			}
			if (contadorPalabras == 0) {
				System.out.println("La palabra no se encuentra en el archivo");
			} else {

				if (contadorPalabras == 1) {
					System.out.println("La palabra se encuentra en el archivo " + contadorPalabras + " vez");
				} else {
					System.out.println("La palabra se encuentra en el archivo " + contadorPalabras + " veces");
				}

			}
			fuente.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}

	public void anularFestival() {
		String linea;
		System.out.println("ANULAR FESTIVAL");

		ArrayList<String> v = new ArrayList<String>();

		try {
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			linea = fuente.readLine();
			while (linea != null) {
				v.add(linea);
				linea = fuente.readLine();
			}
			fuente.close();
			PrintWriter archivo = new PrintWriter(new FileWriter(this.archivo));
			for (int i = 0; i < v.size(); i++) {

				archivo.println(v.get(i).replaceAll("FESTIVAL", "ANULADO"));

			}
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}
	public void mostrarInformacion() {
		String linea;
		System.out.println("Corregir el archivo");

		ArrayList<String> v = new ArrayList<String>();

		try {
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			linea = fuente.readLine();
			while (linea != null) {
				v.add(linea);
				linea = fuente.readLine();
			}
			fuente.close();
			PrintWriter archivo = new PrintWriter(new FileWriter(this.archivo));
			for (int i = 0; i < v.size(); i++) {
				archivo.println(v.get(i).replaceAll("NOMBRE", "").replaceAll("CODIGO", "").replaceAll("LUGAR", "").replaceAll("FESTIVAL", ""));			
				
			}
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}
	
	public void borrarInformacion() {
		String linea;
		System.out.println("Mostrar informacion del cliente");

		ArrayList<String> v = new ArrayList<String>();

		try {
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			linea = fuente.readLine();
			while (linea != null) {
				v.add(linea);
				linea = fuente.readLine();
			}
			fuente.close();
			PrintWriter archivo = new PrintWriter(new FileWriter(this.archivo));
			for (int i = 0; i < v.size(); i++) {

				archivo.println(v.get(i).substring(0,8));
			}
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}
	
	public int integridadArchivo() {
		System.out.println("Comprobar validez del archivo");
		String linea;
		int contadorLineas = 0;

		try {
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			linea = fuente.readLine();
			while (linea != null) {
				contadorLineas++;
				linea = fuente.readLine();
			}
			fuente.close();
			if(contadorLineas>5) {
				System.out.println("La entrada no ha sido creada correctamente");
			}else {
				System.out.println("Entrada creada correctamente");
			}
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
		return contadorLineas;

	}

	

}
