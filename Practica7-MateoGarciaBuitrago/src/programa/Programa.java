package programa;

import java.io.IOException;
import java.util.Scanner;

import clases.FicherosAccesoAleatorio;
import clases.FicherosSecuenciales;

public class Programa {

	static Scanner lector = new Scanner(System.in);
	
	public static void main(String[] args) throws IOException {
	
		System.out.println("Indique el nombre del archivo secuencial");
		String nombre = lector.nextLine();
		FicherosSecuenciales archivo = new FicherosSecuenciales(nombre);
		FicherosAccesoAleatorio ficheroEntrada = new FicherosAccesoAleatorio();
		int opcionGeneral = 0;
		int opcionPersonalizada = 0;
		Scanner lector = new Scanner(System.in);
		
		do {
			menuUno();
			opcionGeneral = lector.nextInt();
			
			switch (opcionGeneral) {
			case 1:
				do {
					
					menuSecuencial();
					opcionPersonalizada = lector.nextInt();
					switch (opcionPersonalizada) {
					case 1:
						archivo.crearArchivo();
						break;
					case 2:
						archivo.visualizarArchivo();
						break;
					case 3:
						archivo.buscarFichero();
						break;
					case 4:
						archivo.anularFestival();
						break;
					case 5:
						archivo.mostrarInformacion();
						break;
					case 6:
						archivo.borrarInformacion();
						break;
					case 7:
						archivo.integridadArchivo();
						break;
					case 8:
						System.exit(opcionPersonalizada);
						break;

					default:
						break;
					}
				} while (opcionPersonalizada != 0);

				break;
			case 2:
				do {
					menuAleatorio();
					opcionPersonalizada = lector.nextInt();
					switch (opcionPersonalizada) {
					case 1:
						ficheroEntrada.rellenarLista();
						break;

					case 2:
						ficheroEntrada.visualizarLista();
						break;

					case 3:
						ficheroEntrada.copiarListaArchivo("datosEntrada");
						break;

					case 4:
						ficheroEntrada.visualizarArchivo("datosEntrada");
						break;

					case 5:
						ficheroEntrada.modificarPrecioArchivo("datosEntrada");
						break;

					case 6:
						ficheroEntrada.mensajeInicio("datosEntrada");
						break;
					case 7:
						ficheroEntrada.mensajeFinal("datosEntrada");
						break;
					case 8:
						
						break;
					case 9:
						
						break;
					
					default:
						break;
					}
				} while (opcionPersonalizada != 0);

				break;
			default:
				break;
			}

		} while (opcionGeneral != 0);
		
		lector.close();
	}

	public static void menuUno() {
		System.out.println("************************************************************************************");
		System.out.println("                                 MENU                                              ");
		System.out.println("1.- FICHEROS SECUENCIALES                                                          ");
		System.out.println("2.- FICHEROS DE ACCESO ALEATORIO                                                   ");
		System.out.println("0.- Salir                                                                          ");
		System.out.println("************************************************************************************");
	}

	public static void menuSecuencial() {
		System.out.println("************************************************************************************");
		System.out.println("                                 MENU                                              ");
		System.out.println("1.- Crear fichero secuencial                                                       ");
		System.out.println("2.- Visualizar fichero secuencial                                                  ");
		System.out.println("3.- Buscar en fichero secuencial                                                   ");
		System.out.println("4.- Anular el festival                                     						   ");
		System.out.println("5.- Mostrar informacion                                            				   ");
		System.out.println("6.- Borrar informacion                                   						   ");
		System.out.println("7.- Comprobar validez de la entrada                             				   ");
		System.out.println("8.- Salir                                                                          ");
		System.out.println("************************************************************************************");
	}

	public static void menuAleatorio() {
		System.out.println("************************************************************************************");
		System.out.println("                                 MENU                                              ");
		System.out.println("1.- Escribir en fichero aleatorio                                                  ");
		System.out.println("2.- Visualizar archivo                                                 ");
		System.out.println("3.- Copiar fichero aleatorio                                                    ");
		System.out.println("4.- Visualizar fichero aleatorio                                             								   ");
		System.out.println("5.- Aplicar ajuste de precios                              												   ");
		System.out.println("6.- Crear un mensaje de inicio                      														   ");
		System.out.println("7.- Crear un mensaje de final                                          									   ");
		System.out.println("8.- Modificar archivo                   															   ");
		System.out.println("0.- Salir                                                                          ");
		System.out.println("************************************************************************************");
	}

}